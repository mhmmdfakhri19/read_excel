package com.task.readexcel.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExcelDataRequest {
    //private String path;
    //private String name;
    private int order;
    private String filepathCustom;
    private int cellNumber;
    private int startRow;
    private int endRow;

    //private String sheetName;
    //private List<ExcelConfig> config;
}
