package com.task.readexcel.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ExcelResponse {
    private String filepath;
}

