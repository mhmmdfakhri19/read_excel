package com.task.readexcel.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExcelConfig {
    private int order;
    //private String column;
    private int cellNumber;
    private int startRow;
    private int endRow;
}
