package com.task.readexcel.service;

import com.task.readexcel.dto.ExcelDataRequest;
import com.task.readexcel.dto.ExcelRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ExcelReadService {

    public ResponseEntity<?> readExcelCustom(ExcelDataRequest excelDataRequest){

        ExcelDataRequest request = new ExcelDataRequest();
        request.setOrder(excelDataRequest.getOrder());
        //int order = excelDataRequest.getOrder();

        try {
            int cellNumber = excelDataRequest.getCellNumber();
            int startRow = excelDataRequest.getStartRow();
            int endRow = excelDataRequest.getEndRow();
            String filepathCustom = excelDataRequest.getFilepathCustom();

            FileInputStream inputStream = new FileInputStream(new File(filepathCustom));

            Workbook workbook = new XSSFWorkbook(inputStream);

            Sheet sheet = workbook.getSheetAt(0);

            Row row;

            for (int i = startRow; i <= endRow; i++){
                row = sheet.getRow(i);
                //data.put(i, new ArrayList<String>());
                Cell cell = row.getCell(cellNumber);
                // for (Cell cells : row) {
                switch (cell.getCellType()) {
                    case STRING:
                        System.out.print(cell.getStringCellValue());
                        System.out.print(" ");
                        break;
                    case NUMERIC:
                        System.out.print(cell.getNumericCellValue());
                        System.out.print(" ");
                        break;
                    case BOOLEAN:
                        System.out.print(cell.getBooleanCellValue());
                        System.out.print(" ");
                        break;
                    case FORMULA:
                        System.out.print(cell.getCellFormula());
                        System.out.print(" ");
                        break;
                }
                //}
                System.out.println(" ");
            }

            log.info("Data berhasil dibaca..");
            return ResponseEntity.status(HttpStatus.OK).body(excelDataRequest);


        } catch (IOException e) {
            e.printStackTrace();
            log.info("Data tidak ada");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error");
        }
    }


    public ResponseEntity<?> readExcel(ExcelRequest excelRequest) {
        //ExcelRequest excels = new ExcelRequest();
        //excels.setFilepath(excels.getFilepath());

        try {
            //filepath = "E:\\Program BOOTCAMP JAVA\\PostgreSQL\\DataExcel.xlsx";
            final String filepath = excelRequest.getFilepath();
            log.info("filePath : {}",filepath);

            FileInputStream inputStream = new FileInputStream(new File(filepath));

            Workbook workbook = new XSSFWorkbook(inputStream);

            Sheet sheet = workbook.getSheetAt(0);

            Map<Integer, List<String>> data = new HashMap<>();
            int i = 0;
            for (Row row : sheet) {
                data.put(i, new ArrayList<String>());
                for (Cell cell : row) {
                    switch (cell.getCellType()) {
                        case STRING:
                            System.out.print(cell.getStringCellValue());
                            System.out.print(" ");
                            break;
                        case NUMERIC:
                            System.out.print(cell.getNumericCellValue());
                            System.out.print(" ");
                            break;
                        case BOOLEAN:
                            System.out.print(cell.getBooleanCellValue());
                            System.out.print(" ");
                            break;
                        case FORMULA:
                            System.out.print(cell.getCellFormula());
                            System.out.print(" ");
                            break;
                    }
                }
                i++;
                System.out.println(" ");
            }
            log.info("Data berhasil dibaca..");
            return ResponseEntity.status(HttpStatus.OK).body("OK");


        } catch (IOException e) {
            e.printStackTrace();
            log.info("Data tidak ada");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error");
        }
    }

}
