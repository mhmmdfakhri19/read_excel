package com.task.readexcel.controller;


import com.task.readexcel.dto.ExcelDataRequest;
import com.task.readexcel.dto.ExcelRequest;
import com.task.readexcel.service.ExcelReadService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/excel")
@Slf4j
@RequiredArgsConstructor
public class ExcelController {

    private final ExcelReadService service;
    //private String filepath;


    @PostMapping
    @RequestMapping("custom")
    ResponseEntity<?> custom(@RequestBody ExcelDataRequest excelDataRequest) throws IOException{
        return service.readExcelCustom(excelDataRequest);
    }

    //@GetMapping
    @PostMapping
    ResponseEntity<?> post(@RequestBody ExcelRequest excelRequest) throws IOException{
        return service.readExcel(excelRequest);
    }

}

// }
//}
